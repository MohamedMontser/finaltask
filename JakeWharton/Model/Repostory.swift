//
//  Repostory.swift
//  JakeWharton
//
//  Created by muhammed gamal on 2/19/20.
//  Copyright © 2020 mohamed montser. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON



class Repository: Object{
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var descriptionRepository: String = ""
    @objc dynamic var owner: Owner? = nil
    
    override static func primaryKey() -> String? {
           return "id"
       }
}

extension Repository {
    static func map(json: JSON) -> Repository {
        let repo = Repository()
        repo.id = json["id"].intValue
        repo.name = json["name"].stringValue
        repo.descriptionRepository = json["description"].stringValue
        repo.owner = Owner.map(json: json["owner"])
        ESRealm.add(repo)
        return repo
    }
}
