//
//  Owner.swift
//  Owner
//
//  Created by muhammed gamal on 2/18/20.
//  Copyright © 2020 mohamed montser. All rights reserved.
//
import Foundation
import RealmSwift
import SwiftyJSON



class Owner: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var avatar_Url: String = ""
    
    override static func primaryKey() -> String? {
           return "id"
       }
}

extension Owner {
    static func map(json: JSON) -> Owner {
        let owner = Owner()
        owner.id = json["id"].intValue
        owner.name = json["login"].stringValue
        owner.avatar_Url = json["avatar_url"].stringValue
        ESRealm.add(owner)
        return owner
    }
}

