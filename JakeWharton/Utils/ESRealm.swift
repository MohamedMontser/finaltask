//
//  ESRealm.swift
//  Thuqey
//
//  Created by Eslam on 1/13/19.
//  Copyright © 2019 magdsoft. All rights reserved.
//

import Foundation
import RealmSwift


class ESRealm {
    static let realm: Realm = try! Realm()
    
    static func objects<Element>(_ type: Element.Type) -> Results<Element> where Element : Object {
        return realm.objects(type)
    }
    
    static func object<Element, KeyType>(ofType type: Element.Type, forPrimaryKey key: KeyType) -> Element? where Element : Object {
        return realm.object(ofType: type, forPrimaryKey: key)
    }
    
    static func object<Element, KeyType>(ofType type: Element.Type, forPrimaryKey key: KeyType, `default`: Element) -> Element where Element : Object {
        if let object = realm.object(ofType: type, forPrimaryKey: key) {
            return object
        }
        add(`default`)
        return `default`
    }
    
    static func add(_ object: Object, update: Bool = true) {
        if realm.isInWriteTransaction {
            realm.add(object, update: .modified)
        }else {
            do {
                try realm.write {
                    realm.add(object, update: .modified)
                }
            } catch let error {
                print("ESRealm failed to add \(object), with error: \(error)")
            }
        }
    }
    
    static func add<S>(_ objects: S, update: Bool = true, realm: Realm = ESRealm.realm) where S : Sequence, S.Element : Object {
        if realm.isInWriteTransaction {
            realm.add(objects, update: .modified)
        }else {
            do {
                try realm.write {
                    realm.add(objects, update: .modified)
                }
            } catch let error {
                print("ESRealm failed to add \(objects), with error: \(error)")
            }
        }
    }
    
    static func `do`(_ block: (_ realm: Realm)->()) {
        do {
            try realm.write {
                block(realm)
            }
        } catch let error {
            print("ESRealm failed to add \(objects), with error: \(error)")
        }
    }
    
    static func commit(_ block: ()->()) {
        do {
            try realm.write {
                block()
            }
        } catch let error {
            print("ESRealm failed to add \(objects), with error: \(error)")
        }
    }
    
    static func delete<Element>(_ objects: Results<Element>) where Element : Object {
        `do` {
            $0.delete(objects)
        }
    }
    
    static func delete(_ object: Object) {
        `do` {
            $0.delete(object)
        }
    }
    
    static func clear<Element>(_ types: [Element.Type]) where Element: Object {
        for type in types {
            delete(realm.objects(type))
        }
    }
}

extension Results {
    public func toAnyCollection() -> AnyRealmCollection<Element> {
        return AnyRealmCollection<Element>(self)
    }
    
    public func toArray() -> [Element] {
        return Array(self)
    }
}

extension List {
    func toArray() -> [Element] {
        return Array(self)
    }
    
    func replaceAll(with data: [Element]) {
        ESRealm.commit {
            self.removeAll()
            self.append(objectsIn: data)
        }
    }
}
