//
//  RepositoryTableViewCell.swift
//  JakeWharton
//
//  Created by muhammed gamal on 2/18/20.
//  Copyright © 2020 mohamed montser. All rights reserved.
//

import UIKit
import SDWebImage
class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet var userImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var DecriptionLabel: UILabel!
    
    func bindData(wihtRepository repository: Repository){
        self.userImageView.sd_setImage(with: URL(string: repository.owner?.avatar_Url ?? ""), placeholderImage: UIImage(named: ""))
        self.titleLabel.text=repository.owner?.name ?? ""
        self.DecriptionLabel.text=repository.descriptionRepository
    }
}
