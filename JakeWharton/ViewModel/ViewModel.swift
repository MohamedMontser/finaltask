//
//  ViewModel.swift
//  JakeWharton
//
//  Created by muhammed gamal on 2/18/20.
//  Copyright © 2020 mohamed montser. All rights reserved.
//

import Foundation
import SwiftyJSON


class ViewModel: NSObject
{
    var repositories: [Repository] = []
    var canLoadMoreOptions: Bool = false
       typealias fetchRepostoriesComplation = ((_ isSuccess: Bool) -> Void)
       
    func fetchRepostories(completion: @escaping fetchRepostoriesComplation, page: Int = 0, per_Page: Int = 0) {
        NetworkManager.shared.requestGetInfo(completion: {
            (isSuccess, data) in
            if isSuccess {
                
                let reposne = (JSON(data))
                let newData = reposne.arrayValue.map(Repository.map)
                if  newData.count > 0{
                    self.canLoadMoreOptions = true
                }else{
                    self.canLoadMoreOptions = false
                }
                newData.forEach({self.repositories.append($0)})
                completion(true)
            }else{
                completion(false)
            }
        }, parameters:  [ "page": page, "per_page": per_Page])
    }

}

