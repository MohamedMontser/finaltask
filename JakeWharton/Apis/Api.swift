//
//  Api.swift
//  JakeWharton
//
//  Created by muhammed gamal on 2/18/20.
//  Copyright © 2020 mohamed montser. All rights reserved.
//

import Foundation
import Alamofire


public protocol updateProgress {
    func onUpdate(progress: Progress)
}

public class NetworkManager: NSObject {
    typealias ResponseCompletion = ((_ isSuccess: Bool, _ response: Any?) -> Void)
    
    let hostAddress = "https://api.github.com/users/JakeWharton/repos"
    
    static let shared = NetworkManager()
    
    var deleget:updateProgress?

    private override init() {
        
    }
    
    func requestGET( parameters: Parameters, completion: @escaping ResponseCompletion) {
        let requestString = hostAddress + "?page=" + "\(parameters["page"]!)" + "&per_page=" + "\(parameters["per_page"]!)"//String(describing:  parameters["per_page"])
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        
        manager.request(requestString, method: .get, parameters: parameters).responseJSON { (response) in
             print("Response from api : \(requestString) , with Identifier : \(response)")
            completion(response.result.isSuccess, response.result.value)
        }.downloadProgress(closure: {
            (progress) in
            self.deleget?.onUpdate(progress: progress)
        })
    }
    
    // MARK: Requests
    func requestGetInfo(completion: @escaping ResponseCompletion, parameters: Parameters) {
        let parameters: Parameters = parameters
        
        self.requestGET( parameters: parameters, completion: completion)
    }
}

