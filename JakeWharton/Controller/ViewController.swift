//
//  ViewController.swift
//  JakeWharton
//
//  Created by muhammed gamal on 2/18/20.
//  Copyright © 2020 mohamed montser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class ViewController: UIViewController {

    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var contianerView: UIView!
    @IBOutlet var loaderView: NVActivityIndicatorView!
    
    let viewModel=ViewModel()
    var page: Int = 0
    let PER_PAGE = 15
    let MAXIMUM: Float = 10.0
    var currentTime: Float = 0.0
    var currentView = ESRealm.objects(Repository.self)
    let REFRESHER: UIRefreshControl=UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NetworkManager.shared.deleget = self
        initTableView()
//        perform(#selector(setProgress), with: nil, afterDelay: 1.0)
        loaderView.startAnimating()
        loaderView.color = UIColor.darkGray
        fetchData()
    }
}

extension ViewController{
    func addRefresher() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = REFRESHER
        }else {
            tableView.addSubview(REFRESHER)
        }
        
        REFRESHER.addTarget(self, action: #selector(self.refreshData), for: .valueChanged)
    }
    
    func getNextPageIfNeede(indexPath: IndexPath){
        if indexPath.row == viewModel.repositories.count - 3 && viewModel.canLoadMoreOptions
        {
            page += 1
            perform(#selector(setProgress), with: nil, afterDelay: 1.0)
                      self.currentTime = 0
                      self.progressView.isHidden = false
            fetchData()
        }
    }
    
    func initTableView(){
        tableView.register(UINib(nibName: "RepositoryTableViewCell", bundle: nil), forCellReuseIdentifier: "RepositoryCell")
        addRefresher()
    }
    
    @objc func refreshData(){
        self.page = 0
        viewModel.repositories.removeAll()
        fetchData()
    }
    
    func fetchData(){
        viewModel.fetchRepostories(completion: {
                   (sucess) in
                   if sucess{
                    self.progressView.isHidden = true
                    self.tableView.reloadData()
                       self.progressView.progress = 1
                       self.currentTime = 9
                   }else{
                       self.displayAlert("check your internet connection", forController: self)
                   }
               }, page: page, per_Page: self.PER_PAGE)
    }
    
    @objc func setProgress(){
        currentTime += 1
        self.progressView.progress = currentTime/MAXIMUM
        if currentTime < MAXIMUM{
        perform(#selector(setProgress), with: nil, afterDelay: 1.0)
        }
    }
    
    public func displayAlert(_ messeg:String,forController controller:UIViewController){
        let alert = UIAlertController(title: "", message: messeg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title:  "ok", style: UIAlertAction.Style.default, handler: nil))
        DispatchQueue.main.async(execute: {
            controller.present(alert, animated: true, completion: nil)
        })
    }
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.count == 0 ?  currentView.count:  viewModel.repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        getNextPageIfNeede(indexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryTableViewCell
        cell.bindData(wihtRepository: viewModel.repositories.count == 0 ?  currentView[indexPath.row]:  viewModel.repositories[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}


extension ViewController: updateProgress{
    func onUpdate(progress: Progress) {
        self.progressView.progress = Float(progress.fractionCompleted)
    }
}
